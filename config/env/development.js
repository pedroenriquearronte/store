'use strict';

module.exports = {
	db: {
		uri: 'mongodb://localhost/mean-dev',
		options: {
			user: '',
			pass: ''
		}
	},
	log: {
		// Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
		format: 'dev',
		// Stream defaults to process.stdout
		// Uncomment to enable logging to a log on the file system
		options: {
			//stream: 'access.log'
		}
	},
	app: {
		title: 'Mobile Solutions'
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || 'APP_ID',
		clientSecret: process.env.FACEBOOK_SECRET || 'APP_SECRET',
		callbackURL: '/auth/facebook/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
		clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
		callbackURL: '/auth/twitter/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || 'APP_ID',
		clientSecret: process.env.GOOGLE_SECRET || 'APP_SECRET',
		callbackURL: '/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || 'APP_ID',
		clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
		callbackURL: '/auth/linkedin/callback'
	},
	github: {
		clientID: process.env.GITHUB_ID || 'APP_ID',
		clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
		callbackURL: '/auth/github/callback'
	},
	paypalConfig: {
		host: 			'api.sandbox.paypal.com',
		// mode: 			'live',
	    // port: 			'',
	    client_id: 		'AegfqjoNLxZ5oC4bVYNVjdgTYg28Z9mgluHMfFzaO2Y7vEAMJMzRzkq-oAj5y3GhgLPgvrEuttjDN1QL',
	    client_secret:  'EELThGx2miodgFvKErMf5T3jDdo7un75z5ELcxe0Z5Ab-DnwU1_ohIgwrdbFTF3HKo0IdwkcBtyAFW0K'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'Unlock International <unlockint64@gmail.com>',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'Gmail',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'unlockint64@gmail.com',
				pass: process.env.MAILER_PASSWORD || 'abc123-+1'
			}
		}
	}
};
