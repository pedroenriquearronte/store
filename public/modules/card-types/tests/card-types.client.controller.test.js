'use strict';

(function() {
	// Card types Controller Spec
	describe('Card types Controller Tests', function() {
		// Initialize global variables
		var CardTypesController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Card types controller.
			CardTypesController = $controller('CardTypesController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Card type object fetched from XHR', inject(function(CardTypes) {
			// Create sample Card type using the Card types service
			var sampleCardType = new CardTypes({
				name: 'New Card type'
			});

			// Create a sample Card types array that includes the new Card type
			var sampleCardTypes = [sampleCardType];

			// Set GET response
			$httpBackend.expectGET('card-types').respond(sampleCardTypes);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.cardTypes).toEqualData(sampleCardTypes);
		}));

		it('$scope.findOne() should create an array with one Card type object fetched from XHR using a cardTypeId URL parameter', inject(function(CardTypes) {
			// Define a sample Card type object
			var sampleCardType = new CardTypes({
				name: 'New Card type'
			});

			// Set the URL parameter
			$stateParams.cardTypeId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/card-types\/([0-9a-fA-F]{24})$/).respond(sampleCardType);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.cardType).toEqualData(sampleCardType);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(CardTypes) {
			// Create a sample Card type object
			var sampleCardTypePostData = new CardTypes({
				name: 'New Card type'
			});

			// Create a sample Card type response
			var sampleCardTypeResponse = new CardTypes({
				_id: '525cf20451979dea2c000001',
				name: 'New Card type'
			});

			// Fixture mock form input values
			scope.name = 'New Card type';

			// Set POST response
			$httpBackend.expectPOST('card-types', sampleCardTypePostData).respond(sampleCardTypeResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Card type was created
			expect($location.path()).toBe('/card-types/' + sampleCardTypeResponse._id);
		}));

		it('$scope.update() should update a valid Card type', inject(function(CardTypes) {
			// Define a sample Card type put data
			var sampleCardTypePutData = new CardTypes({
				_id: '525cf20451979dea2c000001',
				name: 'New Card type'
			});

			// Mock Card type in scope
			scope.cardType = sampleCardTypePutData;

			// Set PUT response
			$httpBackend.expectPUT(/card-types\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/card-types/' + sampleCardTypePutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid cardTypeId and remove the Card type from the scope', inject(function(CardTypes) {
			// Create new Card type object
			var sampleCardType = new CardTypes({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Card types array and include the Card type
			scope.cardTypes = [sampleCardType];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/card-types\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleCardType);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.cardTypes.length).toBe(0);
		}));
	});
}());