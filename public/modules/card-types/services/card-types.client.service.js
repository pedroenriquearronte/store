'use strict';

//Card types service used to communicate Card types REST endpoints
angular.module('card-types').factory('CardTypes', ['$resource',
	function($resource) {
		return $resource('card-types/:cardTypeId', { cardTypeId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);