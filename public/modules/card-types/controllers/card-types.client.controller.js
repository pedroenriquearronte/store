'use strict';

// Card types controller
angular.module('card-types').controller('CardTypesController', ['$scope', '$stateParams', '$location', 'Authentication', 'CardTypes',
	function($scope, $stateParams, $location, Authentication, CardTypes) {
		$scope.authentication = Authentication;

		// Create new Card type
		$scope.create = function() {
			// Create new Card type object
			var cardType = new CardTypes ({
				name: this.name
			});

			// Redirect after save
			cardType.$save(function(response) {
				$location.path('card-types/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Card type
		$scope.remove = function(cardType) {
			if ( cardType ) { 
				cardType.$remove();

				for (var i in $scope.cardTypes) {
					if ($scope.cardTypes [i] === cardType) {
						$scope.cardTypes.splice(i, 1);
					}
				}
			} else {
				$scope.cardType.$remove(function() {
					$location.path('card-types');
				});
			}
		};

		// Update existing Card type
		$scope.update = function() {
			var cardType = $scope.cardType;

			cardType.$update(function() {
				$location.path('card-types/' + cardType._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Card types
		$scope.find = function() {
			$scope.cardTypes = CardTypes.query();
		};

		// Find existing Card type
		$scope.findOne = function() {
			$scope.cardType = CardTypes.get({ 
				cardTypeId: $stateParams.cardTypeId
			});
		};
	}
]);