'use strict';

// Models controller
angular.module('models').controller('ModelsController', ['$modal', '$scope', '$stateParams', '$location', 'Authentication', 'Models', 'Brands', 'ModalService', 
	function($modal, $scope, $stateParams, $location, Authentication, Models, Brands, ModalService) {
		$scope.authentication = Authentication;
		$scope.model = new Models();
		$scope.modalService = ModalService;

		// Create new Model
		$scope.create = function() {
			//Redirect after save
			$scope.model.$save(function(response) {
				$location.path('models');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Model
		$scope.remove = function(model) {
			if (model) { 
				model.$remove();

				for (var i in $scope.models) {
					if ($scope.models [i] === model) {
						$scope.models.splice(i, 1);
					}
				}
			} else {
				$scope.model.$remove(function() {
					$location.path('models');
				});
			}
		};

		// Update existing Model
		$scope.update = function() {
			$scope.model.$update(function() {
				$location.path('models');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Models
		$scope.find = function() {
			$scope.models = Models.query();
		};

		// Find existing Model
		$scope.findOne = function() {
			$scope.textAction = 'Edit';
			$scope.functionAction = $scope.update;
			$scope.brands = Brands.query();

			if ($stateParams.modelId === '0'	) {
				$scope.textAction = 'Create';
				$scope.functionAction = $scope.create;
			} else {
				$scope.getKeys();
			}
		};

		$scope.getKeys = function() {
			$scope.model = Models.get({ 
				modelId: $stateParams.modelId
			});

			$scope.model.$promise.then(function(data) {
	   			$scope.model.brand = data.brand._id;
			});	
		}
		
	}
]);