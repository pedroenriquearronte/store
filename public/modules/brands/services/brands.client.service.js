'use strict';

//Brands service used to communicate Brands REST endpoints
angular.module('brands').factory('Brands', ['$resource',
	function($resource) {
		return $resource('brands/:brandId', { brandId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);