'use strict';

// Brands controller
angular.module('brands').controller('BrandsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Brands',
	function($scope, $stateParams, $location, Authentication, Brands) {
		$scope.authentication = Authentication;

		// Create new Brand
		$scope.create = function() {
			// Create new Brand object
			var brand = new Brands ({
				name: this.name
			});

			// Redirect after save
			brand.$save(function(response) {
				$location.path('brands/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Brand
		$scope.remove = function(brand) {
			
			if (brand) { 
				brand.$remove();

				for (var i in $scope.brands) {
					if ($scope.brands [i] === brand) {
						$scope.brands.splice(i, 1);
					}
				}
			} else {
				$scope.brand.$remove(function() {
					$location.path('brands');
				});
			}
		};

		// Update existing Brand
		$scope.update = function() {
			var brand = $scope.brand;

			brand.$update(function() {
				$location.path('brands/' + brand._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Brands
		$scope.find = function() {
			$scope.brands = Brands.query();
		};

		// Find existing Brand
		$scope.findOne = function() {
			$scope.brand = Brands.get({ 
				brandId: $stateParams.brandId
			});
		};
	}
]);