'use strict';

// Offerts controller
angular.module('offerts').controller('OffertsController', ['$modal', '$scope', '$stateParams', '$location', 'Authentication', 'Offerts', 'util', 'ModalService',
 'Offerts',
	function($modal, $scope, $stateParams, $location, Authentication, Offerts, util, ModalService) {
		$scope.authentication = Authentication;
		$scope.offert = new Offerts ();
		$scope.util = util;
		$scope.modalService = ModalService;

		$scope.create = function() {
			$scope.offert.$save(function(response) {
				$location.path('offerts');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Offert
		$scope.remove = function(offert) {
			if (offert) { 
				offert.$remove();

				for (var i in $scope.offerts) {
					if ($scope.offerts[i] === offert) {
						$scope.offerts.splice(i, 1);
					}
				}
			} else {
				$scope.offert.$remove(function() {
					$location.path('offerts');
				});
			}
		};

		// Update existing Offert
		$scope.update = function() {
			$scope.offert.$update(function() {
				$location.path('offerts');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Offerts
		$scope.find = function() {
			$scope.offerts = Offerts.query();
		};

		// Find existing Offert
		$scope.findOne = function() {
			$scope.textAction = 'Edit';
			$scope.functionAction = $scope.update; 

			//switch create or edit action
			if ($stateParams.offertId === '0') {
				$scope.textAction = 'Create';
				$scope.functionAction = $scope.create; 
			} else {
				$scope.offert = Offerts.get({ 
					offertId: $stateParams.offertId
				});

				$scope.getKeys();
			}
		};

		// load the objects keys
		$scope.getKeys = function() {
			$scope.offert.$promise.then(function(data) {
				$scope.brand = data.model.brand._id;
	   			$scope.offert.country = data.country._id;
	   			$scope.offert.company = data.company._id;
	   			$scope.offert.model = data.model._id;
			});
		}

		$scope.goToRequest = function(offertId) {
			$location.path('requests/create/' + offertId);
		}
	}
]);