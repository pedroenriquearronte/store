'use strict';

//Offerts service used to communicate Offerts REST endpoints
angular.module('offerts').factory('Offerts', ['$resource',
	function($resource) {
		return $resource('offerts/:offertId', { offertId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('offerts').factory("offertsFactory", ['$http', '$q', function($http, $q) { 
	var getOffert = function(countryId, companyId, modelId) {
		var defered = $q.defer(),
			promise = defered.promise,
			string = countryId + '/' + companyId + '/' + modelId; 


		$http.get('offerts/getOffert/' + string).success(function(response) {
			defered.resolve(response);
		}).error(function(error) {
			defered.reject(error.message);
		});	

		return promise;	
	}
    return {
		getOffert  			 
	}	 
}]);