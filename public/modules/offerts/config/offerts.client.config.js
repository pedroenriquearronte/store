'use strict';

// Configuring the Offerts module
angular.module('offerts').run(['Menus',
	function(Menus) {
		// (menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position)
		Menus.addMenuItem('topbar', 'Offerts', 'offerts', 'dropdown', '/#!/offerts', false, ['admin'], null);
		// (menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position)
		Menus.addSubMenuItem('topbar', 'offerts', 'List Offerts', 'offerts', null, true, null, null);
		Menus.addSubMenuItem('topbar', 'offerts', 'New Offert', 'offerts/0/edit', null, false, ['admin'], null);
	
		Menus.addMenuItem('topbar', 'Services', 'offerts', 'dropdown', '/#!/offerts', true, ['user'], null);		
	}
]);