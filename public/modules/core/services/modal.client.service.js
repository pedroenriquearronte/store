angular.module('core').factory('ModalService', ['$modal', 
	function($modal) { 
		var open = function(controller, object) {
		    var modalInstance = $modal.open({
				animation: true,
				templateUrl: 'myModalContent.html',
				controller: 'ModalController',
				resolve: {
					controller: function () {
						return controller;
					},
					object: function () {
						return object;
					}
	  			}
		    });
		}
	    return {
			open
		}	 
	}
]);