angular.module('core').factory('util', ['$http', '$q', 'Authentication', 
	function($http, $q, Authentication) { 		
		var isValidForm = function(fields) {
			for (var i in fields) {
				if (fields[i] == undefined) {
					return false;
				}
			}
			return true;
		}
		var alert = {
			'add' : function(arrayAlert, object) {
				var obj = JSON.parse(JSON.stringify(object));
		    	arrayAlert.push(obj);
	    	},
			'close' : function(arrayAlert, index) {
		    	arrayAlert.splice(index, 1);
	    	}	    	
		}
		var user = {
			'get' : function() {
				return $http.get('/users/me');
			},
			'update' : function(user) {
				return $http.put('/users', user);
			},
			'isAdmin' : function() {
				return Authentication.user ? Authentication.user.roles.indexOf('admin') != -1 : false;
			}
		}
		var getByName = function(route, name) {
			var defered = $q.defer();
			var promise = defered.promise;	

			$http.get('/'+ route +'/getByName/'+ name).success(function(response) {
				defered.resolve(response);
			}).error(function(error) {
				defered.reject(error.message);
			});	

			return promise;
		}
		var getByIdInList = function(list, id) {
    		for (i=0; i<list.length; i++) {
    			if (list[i]._id == id) {
    				return list[i];
    			}
    		}
    		return null;
	    }

	    var getByNameInList = function(list, name) {
    		for (i=0; i<list.length; i++) {
    			if (list[i].name == name) {
    				return list[i];
    			}
    		}
    		return null;
	    }

		var msg = {
			'general' : {
				'unCompleteForm' : {
					'type' : 'danger',
					'msg' : 'Please complete all the fields!'
				},
				'emailSent' : {
					'type' : 'success',
					'msg' : 'The E-mail has been sent'
				}
			},
			'payments' : {
				'less100' : {
					'type' : 'danger',
					'msg' : 'Sorry the minimun value for payment is $100!'
				} 
			},
			'requests' : {
				'noCode' : {
					'type' : 'danger',
					'msg' : 'Sorry, in this moment we do not have this code!'
				},
				'noEnoughMoney' : {
					'type' : 'danger',
					'msg' : 'Sorry, you do not have enough credit!'
				},
				'unCompleteForm' : {
					'type' : 'warning',
					'msg' : 'Please, complete this form!'
				},
				'completeRequest' : {
					'type' : 'success',
					'msg' : 'Congratulations, your request is in process!'
				}
			}
		}
	    return {
			isValidForm,
			alert,
			user,
			msg,
			getByName,
			getByIdInList,
			getByNameInList
		}	 
	}
]);