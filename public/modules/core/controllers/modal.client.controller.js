angular.module('core').controller('ModalController', 
	function (controller, $scope, $modalInstance, object) {
		$scope.text = 'Are you sure do you want delete it?';

		$scope.ok = function () {
			controller.remove(object);
			$modalInstance.close();
		}

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		}
	}
);