'use strict';

angular.module('core').controller('HeaderController', ['$scope', 'Authentication', 'Menus', 'util', 
	function($scope, Authentication, Menus, util) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.menu = Menus.getMenu('topbar');
		$scope.util = util;

		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;

			util.user.get()
			.then(function(res) {
				$scope.user = res.data;
			})
			.catch(function(data) {
				$scope.error = data.message;
			});
		});
	}
]);