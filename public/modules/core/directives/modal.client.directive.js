'use strict';

angular.module('core').directive('modalDirective', [
	function() {
		return {
			restrict: 'E',
			template: 
				    '<script type="text/ng-template" id="myModalContent.html">'+
				        '<div class="modal-header">'+
				            '<h3 class="modal-title">{{ text }}</h3>'+
				        '</div>'+
				        '<div class="modal-footer">'+
				            '<button class="btn btn-primary" ng-click="ok()">OK</button>'+
				            '<button class="btn btn-primary" ng-click="cancel()">Cancel</button>'+
				        '</div>'+
				    '</script>',
		    scope: {
				id: "@",
				text: "@", 
				ok: "&",
				cancel: "&" 
			}
		}
	}
]);