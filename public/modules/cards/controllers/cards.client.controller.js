'use strict';

// Cards controller
angular.module('cards').controller('CardsController', ['$modal', '$scope', '$stateParams', '$location', 'Authentication', 'Cards', 'ModalService', 'cardsFactory',  
	function($modal, $scope, $stateParams, $location, Authentication, Cards, ModalService, cardsFactory) {
		$scope.authentication = Authentication;
		$scope.card = new Cards();
		$scope.modalService = ModalService;
		
		// Create new Card
		$scope.create = function() {
			// Redirect after save
			$scope.card.$save(function(response) {				
				$location.path('cards');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Card
		$scope.remove = function(card) {
			if (card) { 
				card.$remove();

				for (var i in $scope.cards) {
					if ($scope.cards [i] === card) {
						$scope.cards.splice(i, 1);
					}
				}
			} else {
				$scope.card.$remove(function() {
					$location.path('cards');
				});
			}
		};

		// Find a list of Cards
		$scope.find = function() {
			cardsFactory.getCardsByUser($scope.authentication.user._id)
			.then(function(res) {
				$scope.cards = res.data; 
			})
			.catch(function(res) {
				$scope.error = res.data.message;
			});
		};

		// Find existing Card
		$scope.findOne = function() {
			$scope.card = Cards.get({ 
				cardId: $stateParams.cardId
			});
		};

		// Find a list of Card types
		$scope.getYears = function() {			
			var date = new Date();
			var currentYear = date.getFullYear();
			$scope.years = [];

			for (var i = currentYear; i<currentYear+10; i++) {
				$scope.years.push(i);
			}
		}

		// Get a list of months 
		$scope.getMonths = function() {
			$scope.months = ['January', 'February', 'March', 'April', 'May', 'June',
			 'July', 'August', 'September', 'October', 'November', 'December'];
		}	
	}
]);