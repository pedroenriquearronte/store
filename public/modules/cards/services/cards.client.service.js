'use strict';

//Cards service used to communicate Cards REST endpoints
angular.module('cards').factory('Cards', ['$resource',
	function($resource) {
		return $resource('cards/:cardId', { cardId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('cards').factory('cardsFactory', ['$http',
	function($http) {
		var getCardsByUser = function(userId) {
			return $http.get('/cards/byUser/'+userId);
		} 

		return {
			getCardsByUser
		}
	}
]);