'use strict';

// Refunds controller
angular.module('refunds').controller('RefundsController', ['$http', '$scope', '$stateParams', '$location', 'Authentication', 'Refunds', 'Users', 
	function($http, $scope, $stateParams, $location, Authentication, Refunds, Users) {
		$scope.authentication = Authentication;
		$scope.refund = new Refunds ();
		var _this = this;

		this.save = function(res) {
			$scope.refund.user = res.data._id;
			$scope.refund.$save(function(response) {
				$location.path('refunds');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		}	

		// Create new Refund
		$scope.create = function() {
			$scope.user.credit = parseFloat($scope.user.credit) + parseFloat($scope.refund.amount);
			$http.put('/users/updateUser', $scope.user)
			.then(_this.save)
			.catch(function(res) {
				$scope.error = res.data.message;
			});
		};

		// Find a list of Refunds
		$scope.find = function() {
			$scope.refunds = Refunds.query();
		};

		// Find existing Refund
		$scope.findOne = function() {
			$scope.refund = Refunds.get({ 
				refundId: $stateParams.refundId
			});
		};

		$scope.findUser = function() {
			$scope.user = Users.get({ 
				userId: $stateParams.userId
			});
		};
	}
]);