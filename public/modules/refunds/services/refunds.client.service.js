'use strict';

//Refunds service used to communicate Refunds REST endpoints
angular.module('refunds').factory('Refunds', ['$resource',
	function($resource) {
		return $resource('refunds/:refundId', { refundId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);