'use strict';

// request types controller
angular.module('request-types').controller('RequestTypesController', ['$scope', '$stateParams', '$location', 'Authentication', 'RequestTypes', 'util', 
	function($scope, $stateParams, $location, Authentication, RequestTypes, util) {
		$scope.authentication = Authentication;
		$scope.util = util;

		// Create new request type
		$scope.create = function() {
			// Create new request type object
			var requestType = new requestTypes ({
				name: this.name
			});

			// Redirect after save
			requestType.$save(function(response) {
				$location.path('request-types');

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Update existing request type
		$scope.update = function() {
			var requestType = $scope.requestType;

			requestType.$update(function() {
				$location.path('request-types');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of request types
		$scope.find = function() {
			$scope.requestTypes = RequestTypes.query();
		};

		// Find existing request type
		$scope.findOne = function() {
			$scope.requestType = RequestTypes.get({ 
				requestTypeId: $stateParams.requestTypeId
			});
		};
	}
]);