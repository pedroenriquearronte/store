'use strict';

//Setting up route
angular.module('request-types').config(['$stateProvider',
	function($stateProvider) {
		// Request types state routing
		$stateProvider.
		state('listRequestTypes', {
			url: '/request-types',
			templateUrl: 'modules/request-types/views/list-request-type.client.view.html'
		}).
		state('editRequestType', {
			url: '/request-types/:requestTypeId/edit',
			templateUrl: 'modules/request-types/views/edit-request-type.client.view.html'
		});
	}
]);