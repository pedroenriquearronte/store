'use strict';

// Configuring the Articles module
angular.module('request-types').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Request types', 'request-types', 'dropdown', '/request-types(/create)?', false, ['admin']);
		Menus.addSubMenuItem('topbar', 'request-types', 'List Request types', 'request-types', false, ['admin']);
		Menus.addSubMenuItem('topbar', 'request-types', 'Edit Request type', 'request-types/create', false, ['admin']);
	}
]);