'use strict';

//Request types request used to communicate request types REST endpoints
angular.module('request-types').factory('RequestTypes', ['$resource',
	function($resource) {
		return $resource('request-types/:requestTypeId', { requestTypeId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);