'use strict';

// Accounts controller
angular.module('accounts').controller('AccountsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Accounts', 'Users', 'requestsFactory', 
	function($scope, $stateParams, $location, Authentication, Accounts, Users, requestsFactory) {
		$scope.authentication = Authentication;

		// Create new Account
		$scope.create = function() {
			// Create new Account object
			var account = new Accounts ({
				name: this.name
			});

			// Redirect after save
			account.$save(function(response) {
				$location.path('accounts/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Account
		$scope.remove = function(account) {
			if ( account ) { 
				account.$remove();

				for (var i in $scope.accounts) {
					if ($scope.accounts [i] === account) {
						$scope.accounts.splice(i, 1);
					}
				}
			} else {
				$scope.account.$remove(function() {
					$location.path('accounts');
				});
			}
		};

		// Update existing Account
		$scope.update = function() {
			var account = $scope.account;

			account.$update(function() {
				$location.path('accounts/' + account._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Accounts
		$scope.findUsers = function() {
			$scope.users = Users.query();

			$scope.users.$promise.then(function(data) {
				for(var i= 0; i < data.length; i++) {
					if (data[i].roles.indexOf('admin') != -1) {
						$scope.users.splice(i, 1);
					}
				}
			})
		};

		// Find existing Account
		$scope.findOne = function() {
			$scope.account = Accounts.get({ 
				accountId: $stateParams.accountId
			});
		};

		$scope.getRequestsByUser = function(user) {
			requestsFactory.getRequestsByUser(user._id)
			.then(function(res) {
				user.countRequests = res.data.length;
				user.lastBuy = null;

				if (res.data[0] != null) {
					user.lastBuy = res.data[0].created;
				} 
			})
			.catch(function(res) {
				$scope.error = res.data.error;
			});
		}
	}
]);