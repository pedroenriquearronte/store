'use strict';

// Configuring the Articles module
angular.module('accounts').run(['Menus',
	function(Menus) {
		// (menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position)
		Menus.addMenuItem('topbar', 'My Account', 'myAccount', 'dropdown', '#!/payments', false, ['user']);
		// (menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position)
		Menus.addSubMenuItem('topbar', 'myAccount', 'Credit Cards', 'cards');
		Menus.addSubMenuItem('topbar', 'myAccount', 'Payments', 'payments');
		Menus.addSubMenuItem('topbar', 'myAccount', 'Credit', 'payments/create', false, ['user']);
		Menus.addSubMenuItem('topbar', 'myAccount', 'Requests', 'requests', false, ['user']);
	
		//admin menus
		Menus.addMenuItem('topbar', 'Accounts', 'accounts', 'dropdown', '#!/payments', false, ['admin']);
		Menus.addSubMenuItem('topbar', 'accounts', 'Users', 'users', '#!/users');
		Menus.addSubMenuItem('topbar', 'accounts', 'Payments', 'payments', '#!/payments');
		Menus.addSubMenuItem('topbar', 'accounts', 'Refunds', 'refunds', '#!/refunds');

		Menus.addMenuItem('topbar', 'Buy Credit', 'buyCredit', 'dropdown', '/#!/payments/create', true, ['user'], null);
	}
]);