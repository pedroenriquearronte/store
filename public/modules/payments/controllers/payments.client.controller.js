'use strict';

// Payments controller
angular.module('payments').controller('PaymentsController', ['$http', '$scope', '$stateParams', '$location', 'Authentication', 'Payments', 'util', 'paymentsFactory', 
	function($http, $scope, $stateParams, $location, Authentication, Payments, util, paymentsFactory) {
		$scope.authentication = Authentication;
		$scope.payment = new Payments();
		$scope.alerts = [];
		$scope.util = util;
		var _this = this;

		// Create new Payment
		$scope.create = function() {
			if (! paymentsFactory.isValidForm($scope.alerts, $scope.payment, $scope.card)) { return }
				
			switch($scope.payment.type) {
				case 'cards': _this.cardsPayment(); break;
				case 'card': _this.cardPayment(); break;
				// case 'paypal': _this.paypalPayment(); break;
			}			
		}		

		this.create = function(data) {
			$http.post('/payments', data).
			success(function(data, status, headers, config) {
				$location.path('payments');
			}).
			error(function(data, status, headers, config) {
				$scope.error = data.message;
			});
		}

		this.cardsPayment = function() {
			_this.create({'payment' : $scope.payment});
		}

		this.cardPayment = function() {
			var data = {'card': $scope.card, 'payment': $scope.payment, 'save': $scope.save};
			_this.create(data);
		}

		this.paypalPayment = function() {
			_this.create({'payment' : $scope.payment});	
		}
 
		// Find a list of Payments
		$scope.find = function() {
			if (util.user.isAdmin()) {
				$scope.payments = Payments.query();
			} else {
				paymentsFactory.getPaymentsByUser($scope.authentication.user._id)
				.then(function(res) {
					$scope.payments = res.data;
				})
				.catch(function(res) {
					$scope.error = res.data.message;
				});
			}
		};

		// Find existing Payment
		$scope.findOne = function() {
			$scope.payment = Payments.get({ 
				paymentId: $stateParams.paymentId
			});
		};
	}
]);