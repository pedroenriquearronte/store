'use strict';

//Setting up route
angular.module('requests').config(['$stateProvider',
	function($stateProvider) {
		// Requests state routing
		$stateProvider.
		state('listRequests', {
			url: '/requests',
			templateUrl: 'modules/requests/views/list-requests.client.view.html'
		}).
		state('createRequest', {
			url: '/requests/create',
			templateUrl: 'modules/requests/views/create-request.client.view.html'
		}).
		state('editRequest', {
			url: '/requests/:requestId/edit',
			templateUrl: 'modules/requests/views/edit-request.client.view.html'
		}).
		state('detailRequest', {
			url: '/requests/:offertId/:requestId/detail',
			templateUrl: 'modules/requests/views/detail-request.client.view.html'
		}).
		state('confirmEmail', {
			url: '/requests/confirmEmail/:requestId',
			templateUrl: 'modules/requests/views/confirm-request.client.view.html'
		}).
		state('createRequestByOffert', {
			url: '/requests/create/:offertId',
			templateUrl: 'modules/requests/views/create-request.client.view.html'
		}).
		state('createRequestByHome', {
			url: '/requests/createRequestByHome/:requestTypeName',
			templateUrl: 'modules/requests/views/create-request.client.view.html'
		});
	}
]);