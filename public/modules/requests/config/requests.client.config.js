'use strict';

// Configuring the Articles module
angular.module('requests').run(['Menus',
	function(Menus) {
		// (menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position)
		Menus.addMenuItem('topbar', 'Request', 'request', 'dropdown', '/#!/requests/create', true, null, false);
		// (menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position)
		Menus.addSubMenuItem('topbar', 'request', 'List Requests', 'requests', null, false, ['admin'], null);
	}
]);