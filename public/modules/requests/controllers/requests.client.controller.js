'use strict';

// Requests controller
angular.module('requests').controller('RequestsController', ['$http', '$scope', '$stateParams', '$location', 'Authentication', 'Requests', 'Offerts', 'requestsFactory', 'offertsFactory', 'RequestStatuses', 'util', 'Brands',  'RequestTypes', 
	function($http, $scope, $stateParams, $location, Authentication, Requests, Offerts, requestsFactory, offertsFactory, RequestStatuses, util, Brands, RequestTypes) {
		$scope.authentication = Authentication;
		$scope.request = new Requests();
		$scope.alerts = [];
		$scope.util = util;
		$scope.user = null;
		var _this = this;
		var offertPrice = null;

		this.setOffert = function(offert) {
			$scope.request.offert = offert._id;
			offertPrice = offert.price;
			return util.getByName('request-statuses', 'UserCreated');
		}

		this.setRequestStatus = function(requestStatus) {
			$scope.request.requestStatus = requestStatus._id;
<<<<<<< HEAD
			// return paymentStatusesFactory.getByName('PaymentPending');
=======
>>>>>>> beforeDie
			return util.user.get();
		}

		this.decrementUserCredit = function (res) {
			$scope.user = res.data;
			if (parseFloat($scope.user.credit) < parseFloat(offertPrice)) {throw ('noEnoughCredit')}

			$scope.user.credit = parseFloat($scope.user.credit) - parseFloat(offertPrice);
			offertPrice = null;
			return $http.put('/users/updateUser', $scope.user);
		}

		this.save = function () {
			$scope.request.$save(function(response) {
				$location.path('requests/' + $scope.request.offert + '/' + response._id +'/detail');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});	
		}	

		// Create new Request
		$scope.create = function() {		
			var arrayFields = [$scope.country, $scope.company, $scope.brand, $scope.request.model,
			$scope.request.imei, $scope.request.email];

			if (! util.isValidForm(arrayFields)) {
				util.alert.add($scope.alerts, util.msg.general.unCompleteForm);
				return;
			}

			offertsFactory.getOffert($scope.country, $scope.company, $scope.request.model)
			.then(_this.setOffert)
			.then(_this.setRequestStatus)
			.then(_this.decrementUserCredit)	
			.then(_this.save)
			.catch(function(error) {
				if (error == 'noEnoughCredit') {
					util.alert.add($scope.alerts, util.msg.requests.noEnoughMoney);
				} else {
					util.alert.add($scope.alerts, util.msg.requests.noCode);
				}
				return;
			});
		};

		this.update = function(request) {
			request.$update(function() {
				$location.path('requests');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		}

		// Update existing Request
		$scope.update = function() {
			var request = $scope.request;

			// if code is setted change status to AdminReceived
			if (request.code != undefined && request.requestStatus.name == 'AdminSent') {
				util.getByName('request-statuses', 'AdminReceived')
				.then(function(status) {
					request.requestStatus = status._id;
					_this.update(request);
				});
			} else {
				_this.update(request);
			}
		};

		// Find existing Request
		$scope.findOne = function() {
			$scope.offert = Offerts.get({ 
				offertId: $stateParams.offertId
			});

			$scope.offert.$promise.then(function(data) {
				$scope.request = Requests.get({ 
					requestId: $stateParams.requestId
				});	

				$scope.request.$promise.then(function(data) {
					util.alert.add($scope.alerts, util.msg.requests.completeRequest);
				});
			});
		};

		// Find existing Request
		$scope.findOneForEdit = function() {
			$scope.request = Requests.get({ 
				requestId: $stateParams.requestId
			});
			$scope.requestStatuses = RequestStatuses.query();
		};

		// Find a list of Requests
		$scope.find = function() {
			if (util.user.isAdmin()) {
				$scope.requests = Requests.query();
			} else {
				requestsFactory.getRequestsByUser($scope.authentication.user._id)
				.then(function(res) {
					$scope.requests = res.data;
				})
				.catch(function(res) {
					$scope.error = res.data.message;	
				});
			}
		};

	    $scope.sendEmail = function(requestId) {
	    	requestsFactory.sendEmail(requestId)
	    	.then(function(response) {
	            if (response == 'success') {
					util.alert.add($scope.alerts, util.msg.general.emailSent);
					$scope.find();
				}
	        })
	        .catch(function(error) {
	            $scope.error = error;
	        })
	    }

	    $scope.findOffert = function() {
		 	if ($stateParams.offertId !== undefined) {
				var offert = Offerts.get({ offertId: $stateParams.offertId });
				offert.$promise.then(function(data) { 
					$scope.country = data.country._id;
					$scope.company = data.company._id;
					$scope.request.model = data.model._id;
					$scope.brand = data.model.brand._id;
					$scope.request.email = $scope.authentication.user.email;
				});
				util.alert.add($scope.alerts, util.msg.requests.unCompleteForm);
			}

			$scope.requestTypes = RequestTypes.query();
			$scope.brands = Brands.query();
			
			$scope.requestTypes.$promise.then(function(data) {
				if ($stateParams.requestTypeName !== undefined) {
					var requestType = util.getByNameInList(data, $stateParams.requestTypeName);
					$scope.request.requestType = requestType._id;

					if ($stateParams.requestTypeName == 'ICloud') {
						$scope.brands.$promise.then(function(data) {
							var brand = util.getByNameInList(data, 'Iphone');
							$scope.brand = brand._id;
						});
					}
				}
			});
	    }

	    $scope.changeBrandOnICloud = function() {
    		$scope.requestTypes.$promise.then(function(data) {
		    	var requestType = util.getByIdInList(data, $scope.request.requestType);
		    	if (requestType.name == 'ICloud') {
		    		$scope.brands.$promise.then(function(data) {
		    			var brand = util.getByNameInList(data, 'Iphone');		
		    			$scope.brand = brand._id;
		    		});
		    	}
    		});
	    }	    
	}
]); 