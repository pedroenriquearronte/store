'use strict';

// Requests controller
angular.module('requests').controller('ConfirmController', ['$scope', '$http',  '$stateParams', '$location', 'Authentication', 'Requests',
	function($scope, $http, $stateParams, $location, Authentication, Requests) {
		$scope.authentication = Authentication;

		$scope.alerts = [];

		$scope.addAlert = function(type, msg) {
		    $scope.alerts.push({type: type, msg: msg});
	    };

    	$scope.closeAlert = function(index) {
		    $scope.alerts.splice(index, 1);
	    };

		$scope.confirmEmail = function() {
    		$http.get('requests/confirmEmail/' + $stateParams.requestId).success(function(response) {
				if (response == 'success') {
					$scope.addAlert('success', 'The request has been confirmed');
				}
			}).error(function(response) {
				$scope.error = response.message;
			});
	    }
	}
]); 