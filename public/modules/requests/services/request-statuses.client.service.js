'use strict';

angular.module('requests').factory('RequestStatuses', ['$resource',
	function($resource) {
		return $resource('request-statuses/:requestStatusesId', { requestStatusesId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);