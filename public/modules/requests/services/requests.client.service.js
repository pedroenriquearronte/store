'use strict';

//Requests service used to communicate Requests REST endpoints
angular.module('requests').factory('Requests', ['$resource',
	function($resource) {
		return $resource('requests/:requestId', { requestId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('requests').factory('requestsFactory', ['$http', '$q', 'util', 
	function($http, $q, util) { 
		var sendEmail = function(requestId) {
			var defered = $q.defer();
			var promise = defered.promise;	

			$http.get('requests/sendEmail/' + requestId).success(function(response) {
				defered.resolve(response);
			}).error(function(error) {
				defered.reject(error.message);
			});	

			return promise;
		}
		var getRequestsByUser = function(userId) {
			return $http.get('/requests/byUser/'+userId);
		}	

	    return {
			sendEmail,
			getRequestsByUser
		}	 
	}
]);