'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	RequestStatus = mongoose.model('RequestStatus'),
	_ = require('lodash');

/**
 * Show the current Request status
 */
exports.read = function(req, res) {
	res.jsonp(req.requestStatus);
};

/**
 * List of Request statuses
 */
exports.list = function(req, res) { 
	RequestStatus.find().sort('-created').populate('user', 'displayName').exec(function(err, requestStatuses) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(requestStatuses);
		}
	});
};

/**
 * Request status middleware
 */
exports.requestStatusByID = function(req, res, next, id) { 
	RequestStatus.findById(id).populate('user', 'displayName').exec(function(err, requestStatus) {
		if (err) return next(err);
		if (! requestStatus) return next(new Error('Failed to load Request status ' + id));
		req.requestStatus = requestStatus ;
		next();
	});
};

exports.getByName = function(req, res, next) { 
	var name = req.params.name;

	RequestStatus.findOne({ 'name': name }, function (err, requestStatus) {
  		if (err || ! requestStatus) {
			return res.status(400).send(err);
		} else {
			res.jsonp(requestStatus);
		}
	});
};