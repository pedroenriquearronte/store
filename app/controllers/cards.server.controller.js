'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Card = mongoose.model('Card'),
	CardType = mongoose.model('CardType'),
	_ = require('lodash'),
	paypal_api = require('paypal-rest-sdk'),
	config = require('../../config/config'),
	async = require('async');

/**
 * Create a Card
 */
exports.create = function(req, res) {
	var card = req.body;
	async.waterfall([
		function(done) {						
			CardType.findById(card.cardType, function (err, cardType) {
				done(err, cardType);
			});
		},
		function(cardType, done) {
			var savedCard = {
			    "type": cardType.name,
			    "number": card.number,
			    "expire_month": parseInt(card.expireMonth) + 1,
			    "expire_year": card.expireYear,
			    "cvv2": card.cvv2,
			    "first_name": card.firstName,
			    "last_name": card.lastName
			};

			paypal_api.credit_card.create(savedCard, config.paypalConfig, function(paypalError, paypalResponse) {
				done(paypalError, paypalResponse);
			});
		}, 
		function(paypalResponse, done) {
			var _card = new Card(card);
			_card.user = payment.user;
			_card.number = paypalResponse.number;
			_card.validUntil = paypalResponse.valid_until;
			_card.creditCardId = paypalResponse.id;

			_card.save(function(err, res) {
				done(err, res);
			});
		}
	], 
	function(err) {
		if (err) {
			return res.status(400).send({
				message: err
			});
		}
	});
};

/**
 * Show the current Card
 */
exports.read = function(req, res) {
	res.jsonp(req.card);
};

/**
 * Update a Card
 */
exports.update = function(req, res) {
	var card = req.card ;

	card = _.extend(card , req.body);

	card.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(card);
		}
	});
};

/**
 * Delete an Card
 */
exports.delete = function(req, res) {
	var card = req.card ;

	card.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(card);
		}
	});
};

/**
 * List of Cards
 */
exports.list = function(req, res) { 
	Card.find().sort('-created')
	.populate('user', 'displayName')
	.populate('cardType')
	.exec(function(err, cards) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(cards);
		}
	});
};

/**
 * Card middleware
 */
exports.cardByID = function(req, res, next, id) { 
	Card.findById(id).populate('user', 'displayName').exec(function(err, card) {
		if (err) return next(err);
		if (! card) return next(new Error('Failed to load Card ' + id));
		req.card = card;
		next();
	});
};

/**
 * Card authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.card.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};

exports.getCardsByUser = function(req, res) { 
	var userId = req.params.userId;

	Card.find({'user' : userId}).sort('-created')
	.populate('cardType')
	.exec(function(err, cards) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(cards);
		}
	});
}