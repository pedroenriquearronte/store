'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Request = mongoose.model('Request'),
	RequestStatus = mongoose.model('RequestStatus'),
	_ = require('lodash'),
	config = require('../../config/config'),
	nodemailer = require('nodemailer'),
	smtpTransport = nodemailer.createTransport(config.mailer.options),
	async = require('async');


/**
 * Create a Request
 */
exports.create = function(req, res) {
	var request = new Request(req.body);
	request.user = req.user;

	request.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(request);
		}
	});
};

/**
 * Show the current Request
 */
exports.read = function(req, res) {
	res.jsonp(req.request);
};

/**
 * Update a Request
 */
exports.update = function(req, res) {
	var request = req.request;

	request = _.extend(request , req.body);

	request.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(request);
		}
	});
};

/**
 * Delete an Request
 */
exports.delete = function(req, res) {
	var request = req.request;

	request.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(request);
		}
	});
};

/**
 * List of Requests
 */
exports.list = function(req, res) { 
	Request.find().sort('-created')
	.populate('user', 'displayName')
	.populate('offert')
	.populate('requestStatus')
	.populate('paymentStatus')
	.deepPopulate(['offert.country', 'offert.company', 'offert.model', 'offert.model.brand'])
	.exec(function(err, requests) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(requests);
		}
	});
};

/**
 * Request middleware
 */
exports.requestByID = function(req, res, next, id) { 
	Request.findById(id)
	.populate('user', 'displayName')
	.populate('offert')
	.populate('requestStatus')
	.populate('paymentStatus')
	.exec(function(err, request) {
		if (err) return next(err);
		if (! request) return next(new Error('Failed to load Request ' + id));
		req.request = request;
		next();
	});
};

/**
 * Account authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.request.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};


exports.sendEmail = function(req, res, next) {
	var id = req.params.requestId;

	async.waterfall([
		function(done) {
			Request.findById(id)
			.exec(function(err, request) {
				done(err, request);
			});	
		},
		function(request, done) {
			var html = "Hi, this is the response for your request made in "+request.created + 
						" the code is: " + request.code + " please confirm clicking here: " + 
						"http://localhost:3000/#!/requests/confirmEmail/" + request._id;
			var mailOptions = {
				to: request.email,
				from: config.mailer.from,
				subject: request.code,
				html: html
			};

			//send email
			smtpTransport.sendMail(mailOptions, function(err) {
				done(err, request); 
			});			
		},
		function(request, done) {
			// get UserSent status object
			RequestStatus.findOne({ 'name': 'UserSent' }, function (err, requestStatus) {
				done(err, request, requestStatus);						
			});
		},
		function(request, requestStatus, done) {
			request.requestStatus = requestStatus; 	 
			request.save(function(err) {
				done(err);					 
			});
		}
	],
	function(err) {
		if (err) {
			return res.status(400).send(err);
		} 
		res.send('success');
	});
}

exports.confirmEmail = function(req, res, next) {
	var id = req.params.requestId;

	Request.findById(id)
	.exec(function(err, request) {
		if (err || ! request) {
			return res.status(400).send(err);
		}

		// change status
		RequestStatus.findOne({ 'name': 'UserReceived' }, function (err, requestStatus) {
	  		if (err || ! requestStatus) {
				return res.status(400).send(err);
			} else {
				request.requestStatus = requestStatus; 	 
				request.save(function(err) {
					if (err) {
						return res.status(400).send(err);
					} 
				});

				res.send('success');
			}
		});
	});	
}

exports.getRequestsByUser = function(req, res) { 
	var userId = req.params.userId;
	Request.find({'user' : userId}).sort('-created')
	.populate('user', 'displayName')
	.populate('offert')
	.populate('requestStatus')
	.populate('paymentStatus')
	.deepPopulate(['offert.country', 'offert.company', 'offert.model', 'offert.model.brand'])
	.exec(function(err, requests) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(requests);
		}
	});
};
