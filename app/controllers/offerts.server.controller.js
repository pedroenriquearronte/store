'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Offert = mongoose.model('Offert'),
	_ = require('lodash');

/**
 * Create a Offert
 */
exports.create = function(req, res) {
	var offert = new Offert(req.body);
	offert.user = req.user;

	offert.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(offert);
		}
	});
};

/**
 * Show the current Offert
 */
exports.read = function(req, res) {
	res.jsonp(req.offert);
};

/**
 * Update a Offert
 */
exports.update = function(req, res) {
	var offert = req.offert ;

	offert = _.extend(offert , req.body);

	offert.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(offert);
		}
	});
};

/**
 * Delete an Offert
 */
exports.delete = function(req, res) {
	var offert = req.offert ;

	offert.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(offert);
		}
	});
};

/**
 * List of Offerts
 */
exports.list = function(req, res) { 
	Offert.find().sort('-created')
	.populate('user', 'displayName')
	.populate('model')
	.populate('company')
	.populate('country')
	.deepPopulate('model.brand')
	.exec(function(err, offerts) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(offerts);
		}
	});
};

/**
 * Offert middleware
 */
exports.offertByID = function(req, res, next, id) { 
	Offert.findById(id)
	.populate('user', 'displayName')
	.populate('model')
	.populate('company')
	.populate('country')
	.deepPopulate('model.brand')
	.exec(function(err, offert) {
		if (err) return next(err);
		if (! offert) return next(new Error('Failed to load Offert ' + id));
		req.offert = offert;
		next();
	});
};

exports.getOffert = function(req, res, next) {
	var country = req.params.country;
	var company = req.params.company;
	var model = req.params.model;

	// change status
	Offert.findOne({ 'country': country, 'company' : company, 'model' : model }, function (err, offert) {
  		if (err || ! offert) {
			return res.status(400).send(err);
		} else {
			res.jsonp(offert);
		}
	});
}