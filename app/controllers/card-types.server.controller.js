'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	CardType = mongoose.model('CardType'),
	_ = require('lodash');

/**
 * Create a Card type
 */
exports.create = function(req, res) {
	var cardType = new CardType(req.body);
	cardType.user = req.user;

	cardType.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(cardType);
		}
	});
};

/**
 * Show the current Card type
 */
exports.read = function(req, res) {
	res.jsonp(req.cardType);
};

/**
 * Update a Card type
 */
exports.update = function(req, res) {
	var cardType = req.cardType ;

	cardType = _.extend(cardType , req.body);

	cardType.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(cardType);
		}
	});
};

/**
 * Delete an Card type
 */
exports.delete = function(req, res) {
	var cardType = req.cardType ;

	cardType.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(cardType);
		}
	});
};

/**
 * List of Card types
 */
exports.list = function(req, res) { 
	CardType.find().sort('-created').populate('user', 'displayName').exec(function(err, cardTypes) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(cardTypes);
		}
	});
};

/**
 * Card type middleware
 */
exports.cardTypeByID = function(req, res, next, id) { 
	CardType.findById(id).populate('user', 'displayName').exec(function(err, cardType) {
		if (err) return next(err);
		if (! cardType) return next(new Error('Failed to load Card type ' + id));
		req.cardType = cardType ;
		next();
	});
};

/**
 * Card type authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.cardType.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
