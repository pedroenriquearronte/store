'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Brand = mongoose.model('Brand'),
	_ = require('lodash');

/**
 * Create a Brand
 */
exports.create = function(req, res) {
	var brand = new Brand(req.body);
	brand.user = req.user;

	brand.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(brand);
		}
	});
};

/**
 * Show the current Brand
 */
exports.read = function(req, res) {
	res.jsonp(req.brand);
};

/**
 * Update a Brand
 */
exports.update = function(req, res) {
	var brand = req.brand ;

	brand = _.extend(brand , req.body);

	brand.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(brand);
		}
	});
};

/**
 * Delete an Brand
 */
exports.delete = function(req, res) {
	var brand = req.brand ;

	brand.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(brand);
		}
	});
};

/**
 * List of Brands
 */
exports.list = function(req, res) { 
	Brand.find().sort('-created').populate('user', 'displayName').exec(function(err, brands) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(brands);
		}
	});
};

/**
 * Brand middleware
 */
exports.brandByID = function(req, res, next, id) { 
	Brand.findById(id).populate('user', 'displayName').exec(function(err, brand) {
		if (err) return next(err);
		if (! brand) return next(new Error('Failed to load Brand ' + id));
		req.brand = brand;
		next();
	});
};

exports.getByName = function(req, res, next) { 
	var name = req.params.name;

	Brand.findOne({ 'name': name }, function (err, brand) {
  		if (err || ! brand) {
			return res.status(400).send(err);
		} else {
			res.jsonp(brand);
		}
	});
};