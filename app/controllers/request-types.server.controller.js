'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	RequestType = mongoose.model('RequestType'),
	_ = require('lodash');

/**
 * Show the current Request type
 */
exports.read = function(req, res) {
	res.jsonp(req.requestType);
};

/**
 * Update a Request type
 */
exports.update = function(req, res) {
	var requestType = req.requestType;
	requestType = _.extend(requestType , req.body);

	requestType.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(requestType);
		}
	});
};

/**
 * List of Request types
 */
exports.list = function(req, res) { 
	RequestType.find().sort('-created').populate('user', 'displayName').exec(function(err, requestTypes) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(requestTypes);
		}
	});
};

/**
 * Request type middleware
 */
exports.requestTypeByID = function(req, res, next, id) { 
	RequestType.findById(id).populate('user', 'displayName').exec(function(err, requestType) {
		if (err) return next(err);
		if (! requestType) return next(new Error('Failed to load Request type ' + id));
		req.requestType = requestType;
		next();
	});
};

/**
 * Request type authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.requestType.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};

exports.getByName = function(req, res, next) { 
	var name = req.params.name;

	RequestType.findOne({ 'name': name }, function (err, requestType) {
  		if (err || ! requestType) {
			return res.status(400).send(err);
		} else {
			res.jsonp(requestType);
		}
	});
};