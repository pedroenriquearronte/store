'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Refund = mongoose.model('Refund'),
	_ = require('lodash');

/**
 * Create a Refund
 */
exports.create = function(req, res) {
	var refund = new Refund(req.body);
	refund.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(refund);
		}
	});
};

/**
 * Show the current Refund
 */
exports.read = function(req, res) {
	res.jsonp(req.refund);
};

/**
 * Update a Refund
 */
exports.update = function(req, res) {
	var refund = req.refund;
	refund = _.extend(refund, req.body);

	refund.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(refund);
		}
	});
};

/**
 * Delete an Refund
 */
exports.delete = function(req, res) {
	var refund = req.refund ;

	refund.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(refund);
		}
	});
};

/**
 * List of Refunds
 */
exports.list = function(req, res) { 
	Refund.find().sort('-created')
	.populate('user').exec(function(err, refunds) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(refunds);
		}
	});
};

/**
 * Refund middleware
 */
exports.refundByID = function(req, res, next, id) { 
	Refund.findById(id).populate('user', 'displayName').exec(function(err, refund) {
		if (err) return next(err);
		if (! refund) return next(new Error('Failed to load Refund ' + id));
		req.refund = refund ;
		next();
	});
};

/**
 * Refund authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.refund.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
