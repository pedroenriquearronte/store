'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Payment = mongoose.model('Payment'),
	Card = mongoose.model('Card'),
	CardType = mongoose.model('CardType'),
	User = mongoose.model('User'),
	_ = require('lodash'),
	paypal_api = require('paypal-rest-sdk'),
	config = require('../../config/config'),
	async = require('async');

/**
 * Create a Payment
 */

var cardsPayment = function(payment, res) {	
	async.waterfall([
		function(done) {
			var savedCard = {
			    "intent": "sale",
			    "payer": {
			        "payment_method": "credit_card",
			        "funding_instruments": [{
			            "credit_card_token": {
			                "credit_card_id": payment.source
			            }
			        }]
			    },
			    "transactions": [{
			        "amount": {
			            "currency": "USD",
			            "total": payment.amount
			        },
			        "description": "Buy credit for Unlocking Cell International"
			    }]
			};

			paypal_api.payment.create(savedCard, config.paypalConfig, function (err, res) {
				done(err, payment);	
			});
		},
		updateUserCredit,
		savePayment
	],
	function(err) {
        if (err) {
			return res.status(400).send({
				message: 'Some data in wrong, please try again!'
			});
		}
		return res.send('success');
    });
}

var cardPayment = function(payment, card, save, res) {
	async.waterfall([
		function(done) {
			CardType.findById(card.cardType, function (err, cardType) {
				done(err, cardType);
			});
		},
    	function(cardType, done) {
			var create_payment_json = {
				"intent": "sale",
				"payer": {
				    "payment_method": "credit_card",
				    "funding_instruments": [{
				        "credit_card": {
				            "type": cardType.name,
				            "number": card.number,
				            "expire_month": parseInt(card.expireMonth) + 1,
				            "expire_year": card.expireYear,
				            "cvv2": card.cvv2,
				            "first_name": card.firstName,
				            "last_name": card.lastName
				        }
				    }]
			    },
			    "transactions": [{
			        "amount": {
			            "total": payment.amount,
			            "currency": "USD"
			        },
			        "description": "Buy credit for Unlocking Cell International"
			    }]
			};

			paypal_api.payment.create(create_payment_json, config.paypalConfig, function (err, res) {
				done(err, payment, save);
			});
		},
		function(payment, save, done) {
			if (save === 'YES') {
				async.waterfall([
					function(done2) {						
						CardType.findById(card.cardType, function (err, cardType) {
							done2(err, cardType);
						});
					},
					function(cardType, done2) {
						var savedCard = {
						    "type": cardType.name,
						    "number": card.number,
						    "expire_month": parseInt(card.expireMonth) + 1,
						    "expire_year": card.expireYear,
						    "cvv2": card.cvv2,
						    "first_name": card.firstName,
						    "last_name": card.lastName
						};

						paypal_api.credit_card.create(savedCard, config.paypalConfig, function(paypalError, paypalResponse) {
							done2(paypalError, paypalResponse);
						});
					}, 
					function(paypalResponse, done2) {
						var _card = new Card(card);
						_card.user = payment.user;
						_card.number = paypalResponse.number;
						_card.validUntil = paypalResponse.valid_until;
						_card.creditCardId = paypalResponse.id;

						_card.save(function(err, res) {
							done2(err, res);
						});
					}
				], 
				function(err) {
					if (err) {
						done(err);
					}
				});	
			}
			done(null, payment);
		},
		updateUserCredit,
		savePayment
    ], 
    function(err) {
        if (err) {
        	console.log(err);
			return res.status(400).send({
				// message: 'Some data in wrong, please try again!'
				message: err
			});
		}
		return res.send('success');
    });
}

var	paypalPayment = function(payment, req, res) {
	async.waterfall([
		function(done) {
			var create_payment_json = {
			    "intent": "sale",
			    "payer": {
			        "payment_method": "paypal"
			    },
			    "redirect_urls": {
			        "return_url": "http:\/\/localhost\/test\/rest\/rest-api-sdk-php\/sample\/payments\/ExecutePayment.php?success=true",
			        "cancel_url": "http:\/\/localhost\/test\/rest\/rest-api-sdk-php\/sample\/payments\/ExecutePayment.php?success=false"
			    },
			    "transactions": [{
			        "amount": {
			            "currency": "USD",
			            "total": payment.amount
			        },
			        "description": "Buy credit for Unlocking Cell International"
			    }]
			};

			paypal_api.payment.create(create_payment_json, config.paypalConfig, function (err, payment) {
				done(err, payment);		
			});
		},
		function(payment, done) {
			req.session.paymentId = payment.id;
			for(var i=0; i<payment.links.length; i++) {
				var link = payment.links[i];
				if (link.method === 'REDIRECT') {
					req.session.redirectUrl = link.href;
				}
			}
			done();
		}
	],
	function(err) {
        if (err) {
			return res.status(400).send({
				message: 'Some data in wrong, please try again!'
			});
		}

	    // res.redirect(req.session.redirectUrl);
		return res.send(req.session.redirectUrl);
    });
}

var savePayment = function(payment, done) {
	payment.save(function(err) {
		done(err);		
	});
}

var updateUserCredit = function(payment, done) {
	User.findById(payment.user, function(err, user) {
		user.credit += parseFloat(payment.amount);
		user.save(function (err) {
			done(err, payment);
		});
	});
} 

exports.create = function(req, res) {
	var payment = new Payment(req.body.payment);
	payment.user = req.user;

	switch (payment.type) {
		case 'cards': cardsPayment(payment, res); break;
		case 'card': cardPayment(payment, req.body.card, req.body.save, res); break;
		case 'paypal': paypalPayment(payment, req, res); break;
	}
}

/**
 * Show the current Payment
 */
exports.read = function(req, res) {
	res.jsonp(req.payment);
};

/**
 * Update a Payment
 */
exports.update = function(req, res) {
	var payment = req.payment;
	payment = _.extend(payment, req.body);

	payment.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(payment);
		}
	});
};

/**
 * Delete an Payment
 */
exports.delete = function(req, res) {
	var payment = req.payment ;

	payment.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(payment);
		}
	});
};

/**
 * List of Payments
 */
exports.list = function(req, res) { 
	Payment.find().sort('-created').populate('user', 'displayName').exec(function(err, payments) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(payments);
		}
	});
};

/**
 * Payment middleware
 */
exports.paymentByID = function(req, res, next, id) { 
	Payment.findById(id).populate('user', 'displayName').exec(function(err, payment) {
		if (err) return next(err);
		if (! payment) return next(new Error('Failed to load Payment ' + id));
		req.payment = payment ;
		next();
	});
};

/**
 * Payment authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.payment.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};

exports.getPaymentsByUser = function(req, res) { 
	var userId = req.params.userId;
	Payment.find({'user' : userId}).sort('-created')
	.exec(function(err, payments) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(payments);
		}
	});
};