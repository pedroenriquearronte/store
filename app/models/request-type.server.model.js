'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Service type Schema
 */
var RequestTypeSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Service type name',
		trim: true
	},
	time: {
		type: Number,
		default: '',
		required: 'Please fill Service type time',
		trim: true
	},
	price: {
		type: Number,
		default: '',
		required: 'Please fill Service type time',
		trim: true
	}
});

mongoose.model('RequestType', RequestTypeSchema);