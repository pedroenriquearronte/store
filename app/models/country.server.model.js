'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Country Schema
 */
var CountrySchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Country name',
		trim: true
	}
});

mongoose.model('Country', CountrySchema);