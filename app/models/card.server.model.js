'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Card Schema
 */

var CardSchema = new Schema({
	cardType: {
		type: Schema.ObjectId,
		ref: 'CardType',
		required: 'Please select Card type'
	},
	number: {
		type: String,
		default: '',
		required: 'Please fill Card number',
		trim: true
	},
	expireMonth: {
		type: String,
		default: '',
		required: 'Please fill Card expire month',
		trim: true
	},
	expireYear: {
		type: String,
		default: '',
		required: 'Please fill Card expire year',
		trim: true
	},
	cvv2: {
		type: String,
		default: '',
		required: 'Please fill Card cvv2',
		trim: true
	},
	firstName: {
		type: String,
		default: '',
		required: 'Please fill Card first name',
		trim: true
	},
	lastName: {
		type: String,
		default: '',
		required: 'Please fill Card last name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	validUntil: {
		type: Date
	},
	creditCardId : {
		type: String,
		default: '',
		trim: true
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Card', CardSchema);