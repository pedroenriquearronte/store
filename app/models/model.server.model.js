'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Model Schema
 */
var ModelSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Model name',
		trim: true
	},
    brand: {
        type: Schema.ObjectId,
		ref: 'Brand',
		required: 'Please select Model brand',
    }
});

mongoose.model('Model', ModelSchema);