'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Brand Schema
 */
var BrandSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Brand name',
		trim: true
	}
});

mongoose.model('Brand', BrandSchema);