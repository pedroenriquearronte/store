'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Request status Schema
 */
var RequestStatusSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Request status name',
		trim: true
	}
});

mongoose.model('RequestStatus', RequestStatusSchema);