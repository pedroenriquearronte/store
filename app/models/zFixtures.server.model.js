'use strict';

var mongoose = require('mongoose'), 
	fixtures = require('node-mongoose-fixtures'),
	async = require('async');

var Brand = mongoose.model('Brand'),
	User = mongoose.model('User'),
	Country = mongoose.model('Country'),
	Company = mongoose.model('Company'),
	Brand = mongoose.model('Brand'),
	Model = mongoose.model('Model');

var _fixtures = {
	clear : {
		user : function(done) {
			fixtures.reset('User', mongoose, function(err, res) {
				done(err);
			});
		},
		brand : function(done) {
			fixtures.reset('Brand', mongoose, function(err, res) {
				done(err);
			});
		},
		model : function(done) {
			fixtures.reset('Model', mongoose, function(err, res) {
				done(err);
			});
		},
		cardType : function(done) {
			fixtures.reset('CardType', mongoose, function(err, res) {
				done(err);
			});
		},
		company : function(done) {
			fixtures.reset('Company', mongoose, function(err, res) {
				done(err);
			});
		},
		country : function(done) {
			fixtures.reset('Country', mongoose, function(err, res) {
				done(err);
			});
		},
		requestStatus : function(done) {
			fixtures.reset('RequestStatus', mongoose, function(err, res) {
				done(err);
			});
		},
		offert : function(done) {
			fixtures.reset('Offert', mongoose, function(err, res) {
				done(err);
			});
		},
		requestType : function(done) {
			fixtures.reset('RequestType', mongoose, function(err, res) {
				done(err);
			});
		},
	},
	load : {
		user : function(salt, displayName, provider, username, credit, created, roles, password,
		 email, lastName, firstName, _v, done) {
			async.waterfall([
				function(done) {
					fixtures({
					    User: [{'salt': salt, 'displayName': displayName, 'provider': provider,
					    		'username': username, 'credit': credit, 'created': created, 
					    		'roles': roles, 'password': password, 'email': email,
					    		'lastName': lastName, 'firstName': firstName, '_v': _v}]
					}, function(err, res) {
						done(err);
					});		
				}
			], 
			function(err) {done()});
		}, 
		brand : function(brandName, userName, done) {
			async.waterfall([
				function(done) {
					User.findOne({'username' : userName}, function(err, res) {
						done(err, res._id);					
					});
				},
				function(userId, done) {
					fixtures({
					    Brand: [{'name': brandName, 'user': userId}]
					}, function(err, res) {
						done(err);
					});		
				}
			], 
			function(err) {done()});
		},
		model : function(brandName, modelName, userName, done) {
			async.waterfall([
				function(done) {
					Brand.findOne({'name' : brandName}, function(err, res) {
						done(err, res._id);					
					});
				},
				function(brandId, done) {
					User.findOne({'username' : userName}, function(err, res) {
						done(err, brandId, modelName, res._id);					
					});
				},
				function(brandId, modelName, userId, done) {
					fixtures({
					    Model: [{name: modelName, brand: brandId, user: userId}]
					}, function(err, res) {
						done(err);
					});		
				}
			], 
			function(err) {done()});
		},
		cardType : function(cardName, userName, done) {
			async.waterfall([
				function(done) {
					User.findOne({'username' : userName}, function(err, res) {
						done(err, res._id);					
					});
				},
				function(userId, done) {
					fixtures({
					    CardType: [{'name': cardName, 'user': userId}]
					}, function(err, res) {
						done(err);
					});		
				}
			], 
			function(err) {done()});
		},
		company : function(companyName, userName, done) {
			async.waterfall([
				function(done) {
					User.findOne({'username' : userName}, function(err, res) {
						done(err, res._id);					
					});
				},
				function(userId, done) {
					fixtures({
					    Company: [{'name': companyName, 'user': userId}]
					}, function(err, res) {
						done(err);
					});		
				}
			], 
			function(err) {done()});
		},
		country : function(countryName, userName, done) {
			async.waterfall([
				function(done) {
					User.findOne({'username' : userName}, function(err, res) {
						done(err, res._id);					
					});
				},
				function(userId, done) {
					fixtures({
					    Country: [{'name': countryName, 'user': userId}]
					}, function(err, res) {
						done(err);
					});		
				}
			], 
			function(err) {done()});
		},
		requestStatus : function(requestStatusName, userName, done) {
			async.waterfall([
				function(done) {
					User.findOne({'username' : userName}, function(err, res) {
						done(err, res._id);					
					});
				},
				function(userId, done) {
					fixtures({
					    RequestStatus: [{'name': requestStatusName, 'user': userId}]
					}, function(err, res) {
						done(err);
					});		
				}
			], 
			function(err) {done()});
		}, 
		offert : function(countryName, companyName, brandName, modelName, price, time, userName, done) {
			var countryId = null, companyId = null, brandId = null, modelId = null, userId = null;

			async.waterfall([
				function(done) {
					Country.findOne({'name' : countryName}, function(err, res) {
						countryId = res._id; done(err);					
					});
				},
				function(done) {
					Company.findOne({'name' : companyName}, function(err, res) {
						companyId = res._id; done(err);					
					});
				},
				function(done) {
					Brand.findOne({'name' : brandName}, function(err, res) {
						brandId = res._id; done(err);					
					});
				},
				function(done) {
					Model.findOne({'name' : modelName, 'brand' : brandId}, function(err, res) {
						modelId = res._id; done(err);					
					});
				},
				function(done) {
					User.findOne({'username' : userName}, function(err, res) {
						userId = res._id; done(err);					
					});
				},
				function(done) {
					fixtures({
					    Offert: [{'country': countryId, 'company': companyId, 'model': modelId, 'price': price, 'time': time, 'user': userId}]
					}, function(err, res) {
						done(err);
					});		
				}
			], 
			function(err) {done()});
		},
		requestType : function(name, time, price, done) {
			async.waterfall([
				function(done) {
					fixtures({
					    RequestType: [{'name': name, 'time': time, 'price': price}]
					}, function(err, res) {
						done(err);
					});		
				}
			], 
			function(err) {done()});
		},		
	},
	execute : {
		user : function(done) {
			async.waterfall([
				function(done) {
					done(null, 'XdRsI+RuDO8a0VbCQOKEQw==', 'admin admin', 'local',
					'admin', null, null, ['admin'], 
					'D3SV5iCjnQc79HNuqHw3MySYJy/XLJ49sGK0IelMzNrCK5cy5cDUsF9oyAkcVJfTBmoFZ2SRwSwRHvZ3DGnlyg==',
					'unlockint64@gmail.com', 'admin', 'admin', 0)
				},
				_fixtures.load.user,
			],
			function() {done()}); 
		},
		brand : function(done) {
			async.waterfall([
				function(done) {done(null, 'Iphone', 'admin')},
				_fixtures.load.brand,
				function(done) {done(null, 'Samsung', 'admin')},
				_fixtures.load.brand
			],
			function() {done()}); 
		},
		model : function(done) {
			async.waterfall([
				function(done) {done(null, 'Iphone', '6 edge', 'admin')},
				_fixtures.load.model,
				function(done) {done(null, 'Samsung', '6', 'admin')},
				_fixtures.load.model
			],
			function() {done()});
		},
		cardType : function(done) {
			async.waterfall([
				function(done) {done(null, 'visa', 'admin')},
				_fixtures.load.cardType,
				function(done) {done(null, 'mastercard', 'admin')},
				_fixtures.load.cardType
			],
			function() {done()}); 
		},
		company : function(done) {
			async.waterfall([
				function(done) {done(null, 'AT&T', 'admin')},
				_fixtures.load.company,
				function(done) {done(null, 'T-Mobile', 'admin')},
				_fixtures.load.company
			],
			function() {done()}); 
		},
		country : function(done) {
			async.waterfall([
				function(done) {done(null, 'USA', 'admin')},
				_fixtures.load.country,
				function(done) {done(null, 'Mexico', 'admin')},
				_fixtures.load.country
			],
			function() {done()}); 
		},
		requestStatus : function(done) {
			async.waterfall([
				function(done) {done(null, 'UserCreated', 'admin')},
				_fixtures.load.requestStatus,
				function(done) {done(null, 'AdminSent', 'admin')},
				_fixtures.load.requestStatus,
				function(done) {done(null, 'AdminReceived', 'admin')},
				_fixtures.load.requestStatus,
				function(done) {done(null, 'UserSent', 'admin')},
				_fixtures.load.requestStatus,
				function(done) {done(null, 'UserReceived', 'admin')},
				_fixtures.load.requestStatus
			],
			function() {done()}); 
		},
		offert : function(done) {
			async.waterfall([
				function(done) {done(null, 'USA', 'AT&T', 'Iphone', '6 edge', 12.2, 10,'admin')},
				_fixtures.load.offert,
			],
			function() {done()}); 
		},
		requestType : function(done) {
			async.waterfall([
				function(done) {done(null, 'BlackList', 11, 12)},
				_fixtures.load.requestType,
				function(done) {done(null, 'ICloud', 13, 14)},
				_fixtures.load.requestType,
				function(done) {done(null, 'UnLock', 0, 0)},
				_fixtures.load.requestType
			],
			function() {done()}); 
		}, 
	}
}

// clear collections
async.waterfall([
	// _fixtures.clear.user,
	// _fixtures.clear.brand,
	// _fixtures.clear.model,
	// _fixtures.clear.cardType,
	// _fixtures.clear.company,
	// _fixtures.clear.country,
	// _fixtures.clear.requestStatus,
	// _fixtures.clear.offert,
	// _fixtures.clear.requestType
], 
function(err) {});

// load collections
async.waterfall([
	// _fixtures.execute.user,
	// _fixtures.execute.brand,
	// _fixtures.execute.model,
	// _fixtures.execute.cardType,
	// _fixtures.execute.company,
	// _fixtures.execute.country,
	// _fixtures.execute.requestStatus,
	// _fixtures.execute.offert,
	// _fixtures.execute.requestType
], 
function(err) {});