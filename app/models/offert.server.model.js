'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	deepPopulate = require('mongoose-deep-populate');

/**
 * Offert Schema
 */
var OffertSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	country: {
		type: Schema.ObjectId,
		ref: 'Country',
		required: 'Please select Offert country',
	},
	company: {
		type: Schema.ObjectId,
		ref: 'Company',
		required: 'Please select Offert company',
	},
	model: {
		type: Schema.ObjectId,
		ref: 'Model',
		required: 'Please select Offert model',
	},
	price: {
		type: Number,
		default: '',
		required: 'Please fill Offert price',
		trim: true
	},
	time: {
		type: Number,
		default: '',
		required: 'Please fill Offert time',
		trim: true
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

OffertSchema.plugin(deepPopulate, {});
mongoose.model('Offert', OffertSchema);