'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Refund Schema
 */
var RefundSchema = new Schema({
	amount: {
		type: Number,
		default: 0,
		required: 'Please fill Refund amount'
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Refund', RefundSchema);