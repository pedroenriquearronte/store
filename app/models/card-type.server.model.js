'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Card type Schema
 */
var CardTypeSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Card type name',
		trim: true
	}
});

mongoose.model('CardType', CardTypeSchema);