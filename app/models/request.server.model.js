'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	deepPopulate = require('mongoose-deep-populate');

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function(property) {
	return ((this.provider !== 'local' && !this.updated) || property.length);
};

/**
 * Request Schema
 */
var RequestSchema = new Schema({
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	offert: {
		type: Schema.ObjectId,
		ref: 'Offert',
	},
	requestStatus: {
		type: Schema.ObjectId,
		ref: 'RequestStatus',
	},
	requestType: {
		type: Schema.ObjectId,
		ref: 'RequestType',
	},
	imei: {
		type: String,
		trim: true,
		default: '',
	},
	email: {
		type: String,
		trim: true,
		default: '',
		validate: [validateLocalStrategyProperty, 'Please fill in your email'],
		match: [/.+\@.+\..+/, 'Please fill a valid email address']
	},
	code: {
		type: String,
		trim: true,
		default: '',
	},
	created: {
		type: Date,
		default: Date.now
	}
});

RequestSchema.plugin(deepPopulate, {});
mongoose.model('Request', RequestSchema);