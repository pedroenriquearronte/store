'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var refunds = require('../../app/controllers/refunds.server.controller');

	// Refunds Routes
	app.route('/refunds')
		.get(users.hasAuthorization(['admin']), refunds.list)
		.post(users.hasAuthorization(['admin']), refunds.create);

	app.route('/refunds/:refundId')
		.get(users.hasAuthorization(['admin']), refunds.read)
		.put(users.hasAuthorization(['admin']), refunds.update)
		.delete(users.hasAuthorization(['admin']), refunds.delete);

	// Finish by binding the Refund middleware
	app.param('refundId', refunds.refundByID);
};
