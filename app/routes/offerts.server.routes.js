'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var offerts = require('../../app/controllers/offerts.server.controller');

	// Offerts Routes
	app.route('/offerts')
		.get(offerts.list)
		.post(users.hasAuthorization(['admin']), offerts.create);

	app.route('/offerts/:offertId')
		.get(offerts.read)
		.put(users.hasAuthorization(['admin']), offerts.update)
		.delete(users.hasAuthorization(['admin']), offerts.delete);

	app.get('/offerts/getOffert/:country/:company/:model', users.requiresLogin, offerts.getOffert);

	// Finish by binding the Offert middleware
	app.param('offertId', offerts.offertByID);
};
