'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var requestStatuses = require('../../app/controllers/request-statuses.server.controller');

	// Request statuses Routes
	app.route('/request-statuses')
		.get(users.requiresLogin, requestStatuses.list);

	app.route('/request-statuses/:requestStatusId')
		.get(users.requiresLogin, requestStatuses.read);

	app.get('/request-statuses/getByName/:name', requestStatuses.getByName);

	// Finish by binding the Request status middleware
	app.param('requestStatusId', requestStatuses.requestStatusByID);
};
