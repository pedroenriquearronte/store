'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var requestTypes = require('../../app/controllers/request-types.server.controller');

	// Request types Routes
	app.route('/request-types')
		.get(requestTypes.list);

	app.route('/request-types/:requestTypeId')
		.get(users.hasAuthorization(['admin']), requestTypes.read)
		.put(users.hasAuthorization(['admin']), requestTypes.update);

	app.get('/request-types/getByName/:name', requestTypes.getByName);

	// Finish by binding the Request type middleware
	app.param('requestTypeId', requestTypes.requestTypeByID);
};
