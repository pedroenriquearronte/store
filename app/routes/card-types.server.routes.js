'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var cardTypes = require('../../app/controllers/card-types.server.controller');

	// Card types Routes
	app.route('/card-types')
		.get(users.requiresLogin, cardTypes.list)
		.post(users.hasAuthorization(['admin']), cardTypes.create);

	app.route('/card-types/:cardTypeId')
		.get(users.requiresLogin, cardTypes.read)
		.put(users.hasAuthorization(['admin']), cardTypes.update)
		.delete(users.hasAuthorization(['admin']), cardTypes.delete);

	// Finish by binding the Card type middleware
	app.param('cardTypeId', cardTypes.cardTypeByID);
};
