'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var requests = require('../../app/controllers/requests.server.controller');

	// Requests Routes
	app.route('/requests')
		.get(users.requiresLogin, requests.list)
		.post(users.requiresLogin, requests.create);

	app.route('/requests/:requestId')
		.get(users.requiresLogin, requests.read)
		.put(users.hasAuthorization(['admin']), requests.update)
		.delete(users.hasAuthorization(['admin']), requests.delete);

	app.get('/requests/sendEmail/:requestId', requests.sendEmail);
	app.get('/requests/confirmEmail/:requestId', requests.confirmEmail);
	app.get('/requests/byUser/:userId', requests.getRequestsByUser);

	// Finish by binding the Request middleware
	app.param('requestId', requests.requestByID);
};


