'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var companies = require('../../app/controllers/companies.server.controller');

	// Companies Routes
	app.route('/companies')
		.get(companies.list)
		.post(users.hasAuthorization(['admin']), companies.create);

	app.route('/companies/:companyId')
		.get(companies.read)
		.put(users.hasAuthorization(['admin']), companies.update)
		.delete(users.hasAuthorization(['admin']), companies.delete);

	// Finish by binding the Company middleware
	app.param('companyId', companies.companyByID);
};
