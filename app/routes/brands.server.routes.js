'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var brands = require('../../app/controllers/brands.server.controller');

	// Brands Routes
	app.route('/brands') 
		.get(brands.list)
		.post(users.hasAuthorization(['admin']), brands.create);

	app.route('/brands/:brandId')
		.get(brands.read)
		.put(users.hasAuthorization(['admin']), brands.update)
		.delete(users.hasAuthorization(['admin']), brands.delete);

	app.get('/brands/getByName/:name', brands.getByName);

	// Finish by binding the Brand middleware
	app.param('brandId', brands.brandByID);
};
