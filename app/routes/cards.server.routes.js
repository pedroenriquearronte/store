'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var cards = require('../../app/controllers/cards.server.controller');

	// Cards Routes
	app.route('/cards')
		.get(users.requiresLogin, cards.list)
		.post(users.requiresLogin, cards.create);

	app.route('/cards/:cardId')
		.get(users.requiresLogin, cards.read)
		.put(cards.hasAuthorization, cards.update)
		.delete(cards.hasAuthorization, cards.delete);

	app.get('/cards/byUser/:userId', cards.getCardsByUser);

	// Finish by binding the Card middleware
	app.param('cardId', cards.cardByID);
};